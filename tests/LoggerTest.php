<?php

use Playfinder\BookingReport\Logger;
use PHPUnit\Framework\TestCase;

class LoggerTest extends TestCase
{
    private Logger $logger;

    protected function setUp(): void
    {
        $this->logger = new Logger(__DIR__ . '/output.log', 'foo');
    }

    protected function tearDown(): void
    {
        unlink(__DIR__ . '/output.log');
    }

    public function testImplementsLoggerInterface()
    {
        $this->logger->info('Foo', ['rawr' => 'bar']);
        $data = trim(file_get_contents(__DIR__ . '/output.log'));
        $this->assertJson($data);
        $data = json_decode($data, true);
        $this->assertEquals('Foo', $data['message']);
        $this->assertEquals(['rawr' => 'bar'], $data['context']);
        $this->assertEquals('foo', $data['source']);
        $this->assertEquals('info', $data['level']);
        $this->assertArrayHasKey('timestamp', $data);
    }

    public function testImplementsLoggerInterfaceError()
    {
        $this->logger->error('Foo', ['rawr' => 'bar']);
        $data = file_get_contents(__DIR__ . '/output.log');
        $this->assertJson($data);
        $data = json_decode($data, true);
        $this->assertEquals('Foo', $data['message']);
        $this->assertEquals(['rawr' => 'bar'], $data['context']);
        $this->assertEquals('foo', $data['source']);
        $this->assertEquals('error', $data['level']);
        $this->assertArrayHasKey('timestamp', $data);
    }

    public function testMultilinesLogs()
    {
        $this->logger->info('Foo', ['rawr' => 'bar']);
        $this->logger->info('Bar', ['rawr' => 'bar']);
        $data = explode(PHP_EOL, trim(file_get_contents(__DIR__ . '/output.log')));
        $this->assertJson($data[0]);
        $firstLog = json_decode($data[0], true);
        $this->assertEquals('Foo', $firstLog['message']);
        $this->assertEquals(['rawr' => 'bar'], $firstLog['context']);
        $this->assertEquals('foo', $firstLog['source']);
        $this->assertEquals('info', $firstLog['level']);
        $this->assertArrayHasKey('timestamp', $firstLog);

        $this->assertJson($data[1]);
        $secondLog = json_decode($data[1], true);
        $this->assertEquals('Bar', $secondLog['message']);
        $this->assertEquals(['rawr' => 'bar'], $secondLog['context']);
        $this->assertEquals('foo', $secondLog['source']);
        $this->assertEquals('info', $secondLog['level']);
        $this->assertArrayHasKey('timestamp', $secondLog);
    }
}
