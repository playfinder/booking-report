<?php


use PHPUnit\Framework\MockObject\MockObject;
use Playfinder\BookingReport\BookingReporter;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class BookingReporterTest extends TestCase
{
    private BookingReporter $bookingReporter;
    /** @var LoggerInterface|MockObject */
    private LoggerInterface $logger;

    protected function setUp(): void
    {
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->bookingReporter = new BookingReporter($this->logger);
    }

    public function testLogsEvent()
    {
        $this->expectLog(
            'info',
            'CustomerDetails',
            [
                'message' => '',
                'reference' => 'LNCA871287',
                'type' => 'Customer Details',
                'target' => 'walthamforestcouncil'
            ],
        );
        $this->bookingReporter->bookingEvent(
            'LNCA871287',
            BookingReporter::OA_C2,
            'walthamforestcouncil'
        );
    }

    public function testLogsEventWithExtra()
    {
        $this->expectLog(
            'error',
            'Error',
            [
                'message' => 'With extra things',
                'reference' => 'LNCA871287',
                'type' => 'Error',
                'target' => 'sauron',
                'foo' => 'rawr',
            ],
        );
        $this->bookingReporter->bookingEvent(
            'LNCA871287',
            BookingReporter::ERROR,
            'sauron',
            ['foo' => 'rawr'],
            'With extra things'
        );
    }

    private function expectLog(string $level, string $message, array $context)
    {
        $this->logger->expects($this->once())
            ->method('log')
            ->with($level, $message, $context);
    }
}
