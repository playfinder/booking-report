<?php

namespace Playfinder\BookingReport;

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class BookingReporter
{
    public const PLACED = 'Placed';
    public const PAID = 'Paid';
    public const QUEUED = 'Queued';
    public const PROCESSING = 'Processing';
    public const OA_C1 = 'Reserved';
    public const OA_C2 = 'Customer Details';
    public const OA_P = 'Awaiting Confirmation';
    public const OA_UPDATE = 'Updated';
    public const OA_ACCEPT = 'Accepted';
    public const OA_REJECT = 'Rejected';
    public const BOOKED = 'Booked';
    public const ERROR = 'Error';
    public const FAILED = 'Failed';
    public const INFO = 'Info';
    public const CANCELLED = 'Cancelled';
    public const REFUNDED = 'Refunded';

    public const TYPE_MAP = [
        self::PLACED => LogLevel::INFO,
        self::PAID => LogLevel::INFO,
        self::QUEUED => LogLevel::NOTICE,
        self::PROCESSING => LogLevel::NOTICE,
        self::OA_C1 => LogLevel::INFO,
        self::OA_C2 => LogLevel::INFO,
        self::OA_P => LogLevel::INFO,
        self::OA_ACCEPT => LogLevel::INFO,
        self::OA_UPDATE => LogLevel::INFO,
        self::OA_REJECT => LogLevel::WARNING,
        self::BOOKED => LogLevel::INFO,
        self::ERROR => LogLevel::ERROR,
        self::FAILED => LogLevel::CRITICAL,
        self::INFO => LogLevel::INFO,
        self::CANCELLED => LogLevel::WARNING,
        self::REFUNDED => LogLevel::INFO,
    ];

    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function bookingEvent(
        string $reference,
        string $type,
        string $target,
        array $context = [],
        string $message = ''
    ) {
        $this->logger->log(
            self::TYPE_MAP[$type] ?? LogLevel::INFO,
            preg_replace('/[^a-z]+/i', '', $type),
            array_merge($context, [
                'message' => $message,
                'reference' => $reference,
                'type' => $type,
                'target' => $target,
            ])
        );
    }
}
